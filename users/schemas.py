from pydantic import BaseModel
from typing import Optional


class ItemBase(BaseModel):
    title: str
    description: str | None = None


class ItemCreate(ItemBase):
    pass


class Item(ItemBase):
    id: int
    owner_id: int

    class Config:
        orm_mode = True


class UserBase(BaseModel):
    email: str


class UserCreate(UserBase):
    password: str


class RoleBase(BaseModel):
    role: str

class RoleCreate(RoleBase):
    pass

class Role(RoleBase):
    id: Optional[int]
    user_id: Optional[int]

    class Config:
        orm_mode = True


class User(UserBase):
    id: int
    is_active: bool
    items: list[Item] = []
    roles: list[Role] = []

    class Config:
        orm_mode = True
