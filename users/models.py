from sqlalchemy import Boolean, Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship, sessionmaker
from sqlalchemy.dialects.postgresql import ENUM

from .database import Base

Session = sessionmaker()

class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    email = Column(String, unique=True, index=True)
    hashed_password = Column(String)
    is_active = Column(Boolean, default=True)

    roles = relationship("Role", back_populates="user")
    items = relationship("Item", back_populates="owner")

    def __repr__(self) -> str:
        return f"{self.email}"


class Role(Base):
    __tablename__ = "roles"

    id = Column(Integer, primary_key=True, index=True)
    ROLES = ["users", "accounts"]
    role = Column(ENUM(*ROLES, name="role_enum"), default="accounts")
    user_id = Column(Integer, ForeignKey("users.id"))

    user = relationship("User", back_populates="roles", uselist=False)

    def __str__(self):
        return f"{self.role}"


class Item(Base):
    __tablename__ = "items"

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, index=True)
    description = Column(String, index=True)
    owner_id = Column(Integer, ForeignKey("users.id"))

    owner = relationship("User", back_populates="items")
